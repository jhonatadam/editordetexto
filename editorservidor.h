#ifndef EDITORSERVIDOR_H
#define EDITORSERVIDOR_H

#include <QWidget>
#include <QTcpServer>
#include <QDataStream>
#include <QTcpSocket>
#include <QMessageBox>

namespace Ui {
class EditorServidor;
}

class EditorServidor : public QWidget
{
    Q_OBJECT

public:
    explicit EditorServidor(QWidget *parent = 0);
    ~EditorServidor();
    void desfazer();
    void refazer();
    void update();
    void setText(const QString &text);
    QString getText();

private slots:
    void newConnection();
    void disconnectedSocket();
    void setTextE();
    void contentsChange(int,int,int);
    void on_Neg_clicked();
    void on_Ital_pressed();
    void on_Sub_clicked();
    void on_Size_valueChanged(int size);

private:
    Ui::EditorServidor *ui;
    QTcpServer *server;
    QVector<QTcpSocket *> sockets;
    bool changedFlag;

};

#endif // EDITORSERVIDOR_H
