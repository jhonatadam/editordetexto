#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>

#include "editorcliente.h"
#include "editorservidor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void saveFile();
    void saveFileAs();
    void loadFile();
    void EditState();
    void InitState();

private slots:
    void on_actionSair_triggered();
    void on_actionNovo_triggered();
    void on_actionConectar_triggered();
    void on_actionAbrir_triggered();
    void on_actionSalvar_triggered();
    void on_actionDesfazer_triggered();
    void on_actionRefazer_triggered();
    void on_actionAtualizar_triggered();
    void on_actionSalvar_como_triggered();

private:
    Ui::MainWindow *ui;
    EditorCliente *cliente;
    EditorServidor *servidor;
    QWidget *principal;
    QString myFilePath;
    QUdpSocket udpSocket;

};

#endif // MAINWINDOW_H
