#-------------------------------------------------
#
# Project created by QtCreator 2015-05-29T13:06:05
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EditorDeTexto
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    editorcliente.cpp \
    editorservidor.cpp \
    entradatexto.cpp

HEADERS  += mainwindow.h \
    editorcliente.h \
    editorservidor.h \
    entradatexto.h

FORMS    += mainwindow.ui \
    editorcliente.ui \
    editorservidor.ui \
    entradatexto.ui
