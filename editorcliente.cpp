#include <QDebug>
#include <QMessageBox>
#include <QTextBlock>

#include "editorcliente.h"
#include "ui_editorcliente.h"

EditorCliente::EditorCliente(QString ip, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditorCliente)
{
    this->changedFlag = false;

    socket = new QTcpSocket(this);
    socket->connectToHost(ip, 8000);
    if (!socket->waitForConnected(10000))
    {
        QMessageBox::warning(this
                             , "ERRO! D:"
                             , "Não foi possivel se conectar com o servidor");

    }
    else
    {
        ui->setupUi(this);
    }

    connect(socket, SIGNAL(readyRead())
           ,this,   SLOT( Reading() ));

    connect(socket, SIGNAL(disconnected())
           ,this,   SLOT(desconectado()));

    connect(ui->textEdit->document(), SIGNAL(contentsChange(int,int,int))
           ,this,   SLOT(contentsChange(int,int,int)));

}

EditorCliente::~EditorCliente()
{
    socket->disconnectFromHost();
    socket->waitForDisconnected();
    qDebug() << "Destruiu Editor";
    delete ui;
}

void EditorCliente::update()
{
    while (socket->waitForReadyRead(10))
        this->Reading();
}

void EditorCliente::setText(const QString &text)
{
    ui->textEdit->setText(text);
}

QString EditorCliente::getText()
{
    return ui->textEdit->toPlainText();
}

void EditorCliente::refazer()
{
    ui->textEdit->redo();
}

void EditorCliente::desfazer()
{
    ui->textEdit->undo();
}

void EditorCliente::newConnection()
{

}

void EditorCliente::Reading()
{
    changedFlag = true;

    QDataStream ds(socket);
    QString message;
    ds >> message;

    QVector<QStringRef> vs = message.splitRef("\\sep|");

    QString action = vs[0].toString();

    if (action == "add")
    {
        int pos = vs[1].toInt();
        QString dif = vs[2].toString();

        if (dif != "")
        {
            QTextCursor prev_cursor = ui->textEdit->textCursor();
            QTextCursor csr = ui->textEdit->textCursor();
            csr.setPosition(pos);
            ui->textEdit->setTextCursor(csr);
            ui->textEdit->insertPlainText(dif);
            ui->textEdit->setTextCursor(prev_cursor);
        }
    }
    else if (action == "rm")
    {
        int pos = vs[1].toInt();
        int charsRemoved = vs[2].toInt();
        QString text = ui->textEdit->toPlainText();

        text.remove(pos, charsRemoved);
        ui->textEdit->setText(text);
    }
    else if (action == "a|r")
    {
        int pos = vs[1].toInt();
        int charsRemoved = vs[2].toInt();
        QString dif = vs[3].toString();
        QString text = ui->textEdit->toPlainText();

        text.remove(pos, charsRemoved);
        text.insert(pos, dif);
        ui->textEdit->setText(text);

    }
    else if (action == "ud")
    {

        QString text = vs[1].toString();
        ui->textEdit->setText(text);
    }

    changedFlag = false;

}

void EditorCliente::desconectado()
{
    QMessageBox::warning(this
                         , "ERRO! D:"
                         , "Desconectado do servidor");
}

void EditorCliente::contentsChange(int from, int charsRemoved, int charsAdded)
{

    if (!changedFlag)
    {
        QString text = ui->textEdit->toPlainText();
        QDataStream ds;

        // Se houverem caracteres removidos e adicionados
        // simultaneamente
        if (charsRemoved > 0 && charsAdded > 0)
        {
            QString dif;
            for (int i = from; i < from+charsAdded; i++)
            {
                dif.push_back(text[i]);
            }

            ds.setDevice(socket);
            ds << QString("a|r") + QString("\\sep|")
                  + QString::number(from) + QString("\\sep|")
                  + QString::number(charsRemoved) + QString("\\sep|")
                  + dif;
            socket->waitForBytesWritten(10000);

        }
        // Se houverem caracteres removidos no texto
        else if (charsRemoved > 0)
        {
            ds.setDevice(socket);
            ds << QString("rm") + QString("\\sep|")
                  + QString::number(from) + QString("\\sep|")
                  + QString::number(charsRemoved);
            socket->waitForBytesWritten(10000);

        }
        // Se houverem caracteres adicionados no texto
        else if (charsAdded > 0)
        {
            QString dif;
            for (int i = from; i < from+charsAdded; i++)
            {
                dif.push_back(text[i]);
            }

            ds.setDevice(socket);
            ds << QString("add") + QString("\\sep|")
                  + QString::number(from) + QString("\\sep|")
                  + dif;
            socket->waitForBytesWritten(10000);

        }
    }

}

void EditorCliente::on_Size_valueChanged(int size)
{
    ui->textEdit->setFontPointSize(size);
}

void EditorCliente::on_Neg_clicked()
{
    if (ui->textEdit->fontWeight() == 50)
    {
        ui->textEdit->setFontWeight(75);
    }
    else
        ui->textEdit->setFontWeight(50);
}

void EditorCliente::on_Ital_clicked()
{
    if (ui->textEdit->fontItalic())
    {
        ui->textEdit->setFontItalic(false);
    }
    else
        ui->textEdit->setFontItalic(true);
}

void EditorCliente::on_Sub_clicked()
{
    if (ui->textEdit->fontUnderline())
    {
        ui->textEdit->setFontUnderline(false);
    }
    else
        ui->textEdit->setFontUnderline(true);
}
