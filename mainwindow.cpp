#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "entradatexto.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    this->setWindowState(Qt::WindowMaximized);
    principal = this->centralWidget();

    InitState();
}

MainWindow::~MainWindow()
{
    delete cliente;
    delete ui;
}

void MainWindow::saveFile()
{
    if(myFilePath.isEmpty())
        saveFileAs();
    else
    {
        QFile file(myFilePath);
        if (file.open(QIODevice::WriteOnly|QIODevice::Text))
        {
            if (servidor == this->centralWidget())
                file.write(servidor->getText().toUtf8());
            else if (cliente == this->centralWidget())
                file.write(cliente->getText().toUtf8());

            statusBar()->showMessage(tr("Arquivo salvo com sucesso :)"), 3000);
        }
    }

}

void MainWindow::saveFileAs()
{
    this->myFilePath = QFileDialog::getSaveFileName(this);
    if(myFilePath.isEmpty())
        return;
    saveFile();
}

void MainWindow::loadFile()
{
    QString filename = QFileDialog::getOpenFileName(this);
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        if (servidor == this->centralWidget())
            servidor->setText(QString::fromUtf8(file.readAll()));
        else if (cliente == this->centralWidget())
            cliente->setText(QString::fromUtf8(file.readAll()));

        myFilePath = filename;
        statusBar()->showMessage(tr("Arquivo carregado com sucesso"), 3000);
    }
}

void MainWindow::EditState()
{
    this->ui->actionConectar->setEnabled(false);
    this->ui->actionNovo->setEnabled(false);
    this->ui->actionAbrir->setEnabled(true);
    this->ui->actionSalvar->setEnabled(true);
    this->ui->actionRefazer->setEnabled(true);
    this->ui->actionDesfazer->setEnabled(true);
    this->ui->actionAtualizar->setEnabled(true);
    this->ui->actionSalvar_como->setEnabled(true);
}

void MainWindow::InitState()
{
    this->ui->actionConectar->setEnabled(true);
    this->ui->actionNovo->setEnabled(true);
    this->ui->actionAbrir->setEnabled(false);
    this->ui->actionSalvar->setEnabled(false);
    this->ui->actionRefazer->setEnabled(false);
    this->ui->actionDesfazer->setEnabled(false);
    this->ui->actionAtualizar->setEnabled(false);
    this->ui->actionSalvar_como->setEnabled(false);
}

void MainWindow::on_actionSair_triggered()
{
    if (cliente == this->centralWidget())
    {
        this->setCentralWidget(principal);
        InitState();
    }
    else if (servidor == this->centralWidget())
    {
        this->setCentralWidget(principal);
        InitState();
    }
    else if (principal == this->centralWidget())
    {
        this->close();
    }
}

void MainWindow::on_actionNovo_triggered()
{
    if (cliente == this->centralWidget())
    {
        QMessageBox::warning(this
                             , "ERRO! D:"
                             , "Você deve sair do "
                               "documento atual para "
                               "criar um novo");

    }
    else if (servidor == this->centralWidget())
    {

    }
    else if (principal == this->centralWidget())
    {
        principal = this->takeCentralWidget();
        servidor = new EditorServidor;
        this->setCentralWidget(servidor);

        EditState();
    }

}

void MainWindow::on_actionConectar_triggered()
{
    EntradaTexto ei("IP:");
    ei.exec();
    QString ip = ei.getIp();

    if (ip == "" && ei.getOk())
    {
        QMessageBox::warning(this
                             , "ERRO! D:"
                             , "Você deve inserir um ip para se conectar");
    }
    else if (ip != "" && ei.getOk())
    {
        principal = this->takeCentralWidget();
        cliente = new EditorCliente(ip);
        this->setCentralWidget(cliente);

        EditState();
    }
}

void MainWindow::on_actionAbrir_triggered()
{
    this->loadFile();
}

void MainWindow::on_actionSalvar_triggered()
{
    this->saveFile();
}

void MainWindow::on_actionDesfazer_triggered()
{

    if (cliente == this->centralWidget())
    {
        cliente->desfazer();
    }
    else if (servidor == this->centralWidget())
    {
        servidor->desfazer();
    }
}

void MainWindow::on_actionRefazer_triggered()
{
    if (cliente == this->centralWidget())
    {
        cliente->refazer();
    }
    else if (servidor == this->centralWidget())
    {
        servidor->refazer();
    }
}

void MainWindow::on_actionAtualizar_triggered()
{
    if (servidor == this->centralWidget())
    {
        servidor->update();
    }
    else if (cliente == this->centralWidget())
    {
        cliente->update();
    }
}

void MainWindow::on_actionSalvar_como_triggered()
{
    saveFileAs();
}
