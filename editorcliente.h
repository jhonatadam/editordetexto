#ifndef EDITORCLIENTE_H
#define EDITORCLIENTE_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class EditorCliente;
}

class EditorCliente : public QWidget
{
    Q_OBJECT

public:
    explicit EditorCliente(QString ip, QWidget *parent = 0);
    ~EditorCliente();

    void update();
    void setText(const QString &text);
    QString getText();
    void refazer();
    void desfazer();

private slots:
    void newConnection();
    void Reading();
    void desconectado();
    void contentsChange(int,int,int);

    void on_Size_valueChanged(int arg1);

    void on_Neg_clicked();

    void on_Ital_clicked();

    void on_Sub_clicked();

private:
    Ui::EditorCliente *ui;
    QTcpSocket *socket;
    bool changedFlag;

};

#endif // EDITORCLIENTE_H
