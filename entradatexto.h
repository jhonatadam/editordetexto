#ifndef ENTRADAIP_H
#define ENTRADAIP_H

#include <QDialog>

namespace Ui {
class EntradaIP;
}

class EntradaTexto : public QDialog
{
    Q_OBJECT

public:
    explicit EntradaTexto(QString nomeTexto, QWidget *parent = 0);
    ~EntradaTexto();

    QString getIp() const;
    void setIp(const QString &value);
    bool getOk() const;
    void setOk(bool value);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::EntradaIP *ui;
    QString texto;
    bool ok;

};

#endif // ENTRADAIP_H
