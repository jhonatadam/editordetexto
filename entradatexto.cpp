#include "entradatexto.h"
#include "ui_entradatexto.h"

EntradaTexto::EntradaTexto(QString nomeTexto, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntradaIP)
{
    ui->setupUi(this);
    ui->lNomeTexto->setText(nomeTexto);
    this->ok = false;
}

EntradaTexto::~EntradaTexto()
{
    delete ui;
}

void EntradaTexto::on_buttonBox_accepted()
{
    this->texto =  ui->ldIP->text();
    this->ok = true;
}
bool EntradaTexto::getOk() const
{
    return ok;
}

void EntradaTexto::setOk(bool value)
{
    ok = value;
}

QString EntradaTexto::getIp() const
{
    return texto;
}

void EntradaTexto::setIp(const QString &value)
{
    texto = value;
}


void EntradaTexto::on_buttonBox_rejected()
{
    this->ok = false;
}
