#include "editorservidor.h"
#include "ui_editorservidor.h"

EditorServidor::EditorServidor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditorServidor)
{
    changedFlag = false;
    ui->setupUi(this);

    server = new QTcpServer(this);

    if (!server->listen(QHostAddress::Any, 8000))
    {
        QMessageBox::warning(this
                             , "ERRO! D:"
                             , "Não foi possivel iniciar o servidor");
        exit(1);
    }

    connect( server, SIGNAL(newConnection())
           ,   this,   SLOT(newConnection()));
    connect(ui->textEdit->document(), SIGNAL(contentsChange(int,int,int))
           ,this,   SLOT(contentsChange(int,int,int)));

}

EditorServidor::~EditorServidor()
{
    delete server;
    for (auto sckt : sockets)
        delete sckt;
    delete ui;
}

void EditorServidor::desfazer()
{
    ui->textEdit->document()->undo();
}

void EditorServidor::refazer()
{
    ui->textEdit->document()->redo();
}

void EditorServidor::update()
{
    QString text = ui->textEdit->toPlainText();
    QDataStream ds;
    QString message;

    message = QString("ud") + QString("\\sep|")
            + text;

    for (int i = 0; i < sockets.size(); i++)
    {
        ds.setDevice(sockets[i]);
        ds << message;
        sockets[i]->waitForBytesWritten(10000);
    }

}

void EditorServidor::setText(const QString &text)
{
    ui->textEdit->setText(text);
}

QString EditorServidor::getText()
{
    return ui->textEdit->toPlainText();
}

void EditorServidor::newConnection()
{
    QTcpSocket *socket = server->nextPendingConnection();

    connect( socket, SIGNAL(readyRead())
           ,   this,   SLOT(setTextE()));
    connect( socket, SIGNAL(disconnected())
           ,   this,   SLOT(disconnectedSocket()));

    sockets.push_back(socket);
    QDataStream ds(socket);
    ds << QString("add") + QString("\\sep|")
          + QString::number(0) + QString("\\sep|")
          + ui->textEdit->toPlainText();
    socket->waitForBytesWritten();

}

void EditorServidor::disconnectedSocket()
{
    QObject *qo = sender();
    sockets.removeOne((QTcpSocket*) qo);

    qDebug() << "Alguém saiu";
}

void EditorServidor::setTextE()
{
    changedFlag = true;

    QObject *qo = sender();
    QString text = ui->textEdit->toPlainText();
    QString message;

    QDataStream ds((QTcpSocket*) qo);
    ds >> message;

    QVector<QStringRef> vs = message.splitRef("\\sep|");
    QString action = vs[0].toString();
    if (action == "add")
    {
        int pos = vs[1].toInt();
        QString dif = vs[2].toString();

        text.insert(pos, dif);
        ui->textEdit->setText(text);

        for (int i = 0; i < sockets.size(); i++)
        {
            if (sockets[i] != qo)
            {
                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }
    }
    else if (action == "rm")
    {
        int pos = vs[1].toInt();
        int charsRemoved = vs[2].toInt();

        text.remove(pos, charsRemoved);
        ui->textEdit->setText(text);

        for (int i = 0; i < sockets.size(); i++)
        {
            if (sockets[i] != qo)
            {
                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }
    }
    else if (action == "a|r")
    {
        int pos = vs[1].toInt();
        int charsRemoved = vs[2].toInt();
        QString dif = vs[3].toString();

        text.remove(pos, charsRemoved);
        text.insert(pos, dif);
        ui->textEdit->setText(text);

        for (int i = 0; i < sockets.size(); i++)
        {
            if (sockets[i] != qo)
            {
                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }
    }

    changedFlag = false;
}

void EditorServidor::contentsChange(int from, int charsRemoved, int charsAdded)
{

    if (!changedFlag)
    {
        QString text = ui->textEdit->toPlainText();
        QDataStream ds;
        QString message;

        if (charsRemoved > 0 && charsAdded > 0)
        {
            QString dif;
            for (int i = from; i < from+charsAdded; i++)
                dif.push_back(text[i]);

            message = QString("a|r") + QString("\\sep|")
                    + QString::number(from) + QString("\\sep|")
                    + QString::number(charsRemoved) + QString("\\sep|")
                    + dif;

            for (int i = 0; i < sockets.size(); i++)
            {
                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }
        // Se houverem caracteres removidos no texto
        else if (charsRemoved > 0)
        {
            message = QString("rm") + QString("\\sep|")
                    + QString::number(from) + QString("\\sep|")
                    + QString::number(charsRemoved);

            for (int i = 0; i < sockets.size(); i++)
            {
                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }
        // Se houverem caracteres adicionados no texto
        else if (charsAdded > 0)
        {
            QString dif;
            for (int i = from; i < from+charsAdded; i++)
                dif.push_back(text[i]);

            message = QString("add") + QString("\\sep|")
                    + QString::number(from) + QString("\\sep|")
                    + dif;

            for (int i = 0; i < sockets.size(); i++)
            {

                ds.setDevice(sockets[i]);
                ds << message;
                sockets[i]->waitForBytesWritten(10000);
            }
        }

    }

}

void EditorServidor::on_Neg_clicked()
{
    if (ui->textEdit->fontWeight() == 50)
    {
        ui->textEdit->setFontWeight(75);
    }
    else
        ui->textEdit->setFontWeight(50);
}

void EditorServidor::on_Ital_pressed()
{
    if (ui->textEdit->fontItalic())
    {
        ui->textEdit->setFontItalic(false);
    }
    else
        ui->textEdit->setFontItalic(true);
}

void EditorServidor::on_Sub_clicked()
{
    if (ui->textEdit->fontUnderline())
    {
        ui->textEdit->setFontUnderline(false);
    }
    else
        ui->textEdit->setFontUnderline(true);
}

void EditorServidor::on_Size_valueChanged(int size)
{
    ui->textEdit->setFontPointSize(size);
}
